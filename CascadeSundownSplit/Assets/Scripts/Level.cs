﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    public int startPos = 2;
    bool isRandom = false;
    public List<Goal> goals = new List<Goal>(3);
	
	void OnDestroy ()
    {
        for (int i = 0; i < goals.Count; i++)
        {
            if (goals[i] != null) Destroy(goals[i].gameObject);
        }
	}

    void Start ()
    {
        startPos = isRandom ? Random.Range(0, 5) : startPos;
        GenerateGoals();
        PositionStartImage();

        GameController.Instance.CheckComplete();
    }

    void PositionStartImage ()
    {
        GameController.Instance.startImage.GetComponent<RectTransform>().position = GameController.Instance.rows[0][startPos].GetComponent<RectTransform>().position;
        GameController.Instance.startImage.GetComponent<RectTransform>().position += new Vector3(0, Goal.size + (GameController.Instance.gridSpacing/3.0f), 0);
    }

    void GenerateGoals ()
    {
        goals.Clear();

        List<int> takenPos = new List<int>();
        List<int> takenCol = new List<int>();

        for (int i = 0; i < 3; i++)
        {
            int abort = 100;

            int pos = Random.Range(0, 5);
            int colourIndex = Random.Range(4, 7);

            while (takenPos.Contains(pos) && abort > 0)
            {
                pos = Random.Range(0, 5);
                abort--;
            }
            takenPos.Add(pos);

            while (takenCol.Contains(colourIndex) && abort > 0)
            {
                colourIndex = Random.Range(4, 7);
                abort--;
            }
            takenCol.Add(colourIndex);

            Goal newGoal = new GameObject().AddComponent<Goal>().SetGoal(pos, colourIndex);
            goals.Add(newGoal);
        }
    }
}
