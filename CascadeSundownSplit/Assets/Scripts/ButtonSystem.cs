﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSystem : MonoBehaviour
{
    bool left = false;
    bool right = false;

    public Button LeftButton;
    public Button RightButton;

    public void OnClick_Left ()
    {
        if (left)
        {
            left = false;
            LeftButton.image.color = Color.white;
            return;
        }

        LeftButton.image.color = Color.cyan;
        left = true;
        right = false;
        RightButton.image.color = Color.white;
    }

    public void OnClick_Right ()
    {
        if (right)
        {
            right = false;
            RightButton.image.color = Color.white;
            return;
        }

        RightButton.image.color = Color.cyan;
        right = true;
        left = false;
        LeftButton.image.color = Color.white;
    }

    public void OnClick_Tile (Button thisButton)
    {
        if (!right && !left)
        {
            thisButton.GetComponent<Tile>().SetAction(Tile.ActionType.None);
            GameController.Instance.CheckComplete();
            return;
        }

        if (right)
        {
            right = false;
            RightButton.image.color = Color.white;
            thisButton.GetComponent<Tile>().SetAction(Tile.ActionType.Right);
        }
        else
        {
            left = false;
            LeftButton.image.color = Color.white;
            thisButton.GetComponent<Tile>().SetAction(Tile.ActionType.Left);
        }
        GameController.Instance.CheckComplete();
    }
}
