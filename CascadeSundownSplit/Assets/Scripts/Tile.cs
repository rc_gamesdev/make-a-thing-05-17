﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    public enum ActionType { None, Left, Right };
    public ActionType Action = ActionType.None;
    public LightColour Colour = LightColour.None;

    void Update ()
    {
        if (GetComponent<Image>().color != GameController.Instance.LightColours[(int)Colour])
        {
            GetComponent<Image>().color = GameController.Instance.LightColours[(int)Colour];
        }
    }

    public void SetAction (ActionType action)
    {
        if (action == Action) return;

        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }

        Action = action;

        switch (action)
        {
            case ActionType.None:
                break;
            case ActionType.Left:
                GameObject leftMarker = Instantiate(GameController.Instance.LeftArrowPrefab, transform.position, GameController.Instance.LeftArrowPrefab.transform.rotation) as GameObject;
                leftMarker.transform.SetParent(transform);
                break;
            case ActionType.Right:
                GameObject rightMarker = Instantiate(GameController.Instance.RightArrowPrefab, transform.position, GameController.Instance.RightArrowPrefab.transform.rotation) as GameObject;
                rightMarker.transform.SetParent(transform);
                break;
        }
    }
}
