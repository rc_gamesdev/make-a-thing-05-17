﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goal : MonoBehaviour
{
    public static float size = 45;
    Image image;
    public int position;
    public int colourIndex;

	// Use this for initialization
	public Goal SetGoal (int pos, int col)
    {
        position = pos;
        colourIndex = col;

		if (image == null)
        {
            image = gameObject.AddComponent<Image>();
        }
        image.sprite = GameController.Instance.goalSprite;
        image.color = GameController.Instance.LightColours[colourIndex];

        GetComponent<RectTransform>().sizeDelta = new Vector2(size*1.5f, size*1.5f);
        GetComponent<RectTransform>().position = GameController.Instance.rows[GameController.Instance.rows.Count - 1][pos].GetComponent<RectTransform>().position;
        GetComponent<RectTransform>().position -= new Vector3(0, (GameController.Instance.gridSpacing/3.0f) + size, 0);

        transform.SetParent(GameController.Instance.PlaySpace.transform);
        return this;
	}
}
