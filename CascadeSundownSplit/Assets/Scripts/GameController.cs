﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum LightColour
{
    White,
    Purple,
    Orange,
    Green,
    Blue,
    Red,
    Yellow,
    None
}

public class GameController : MonoBehaviour
{
    public float gridSpacing = 3.0f;

    public List<GameObject> rowParents;
    public List<List<Tile>> rows;
    public List<Color> LightColours;
    public List<Vector2> Results;

    public Level currentLevel;
    bool hasWon = false;

    public Sprite goalSprite;
    public Image startImage;
    public Button SubmitBtn;

    public GameObject LeftArrowPrefab;
    public GameObject RightArrowPrefab;

    public GameObject PlaySpace;
    public static GameController Instance;

    const string resetText = "Reset";
    const string nextText = "Next";

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start ()
    {
        if (rows==null || rows.Count == 0)
        {
            rows = new List<List<Tile>>();

            for (int i = 0; i < rowParents.Count; i++)
            {
                List<Tile> tempList = new List<Tile>();
                tempList.AddRange(rowParents[i].GetComponentsInChildren<Tile>());
                rows.Add(tempList);
            }
        }


		if (rows != null && rows.Count != 0)
        {
            int posOfCenterRow = rows.Count / 2;

            for (int i = 0; i < rows.Count; i++)
            {
                rowParents[i].GetComponent<RectTransform>().localPosition += new Vector3(0, (posOfCenterRow - i) * gridSpacing, 0);

                int posOfCenterColumn = rows[i].Count / 2;

                for (int j = 0; j < rows[i].Count; j++)
                {
                    rows[i][j].GetComponent<RectTransform>().localPosition -= new Vector3((posOfCenterColumn-j)*gridSpacing, 0, 0);
                }
            }
        }

        if (currentLevel == null)
        {
            currentLevel = new GameObject().AddComponent<Level>();
            currentLevel.name = "Level";
        }

        Results = new List<Vector2>();

        if (SubmitBtn != null) SubmitBtn.GetComponentInChildren<Text>().text = resetText;
	}
	
    public void CheckComplete ()
    {
        for (int i = 0; i < rows.Count; i++)
        {
            for (int j = 0; j < rows[i].Count; j++)
            {
                rows[i][j].Colour = LightColour.None;
            }
        }

        Results.Clear();

        for (int i = 0; i < rows.Count; i++)
        {
            if (i == 0)
            {
                //Row 0 contains just the base colours, red-blue-yellow
                rows[i][currentLevel.startPos - 1].Colour = LightColour.Red;
                rows[i][currentLevel.startPos].Colour = LightColour.Blue;
                rows[i][currentLevel.startPos + 1].Colour = LightColour.Yellow;

                for (int j = currentLevel.startPos - 1; j <= currentLevel.startPos + 1; j++)
                {
                    switch (rows[i][j].Action)
                    {
                        case Tile.ActionType.None:
                            rows[i + 1][j].Colour = AddColours(rows[i + 1][j].Colour, rows[i][j].Colour);
                            break;
                        case Tile.ActionType.Left:
                            if (j <= 0) break;

                            rows[i + 1][j - 1].Colour = AddColours(rows[i + 1][j - 1].Colour, rows[i][j].Colour);
                            break;
                        case Tile.ActionType.Right:
                            if (j >= rows[i].Count - 1) break;

                            rows[i + 1][j + 1].Colour = AddColours(rows[i + 1][j + 1].Colour, rows[i][j].Colour);
                            break;
                    }
                }
            }
            else
            {
                for (int j = 0; j < rows[i].Count; j++)
                {
                    if (rows[i][j].Colour == LightColour.None) continue;

                    switch (rows[i][j].Action)
                    {
                        case Tile.ActionType.None:
                            if (i == rows.Count - 1)
                            {
                                Results.Add(new Vector2(j, (int)rows[i][j].Colour));
                                break;
                            }
                            rows[i + 1][j].Colour = AddColours(rows[i + 1][j].Colour, rows[i][j].Colour);
                            break;
                        case Tile.ActionType.Left:
                            if (j <= 0) break;
                            if (i == rows.Count - 1)
                            {
                                Results.Add(new Vector2(j - 1, (int)rows[i][j].Colour));
                                break;
                            }
                            rows[i + 1][j - 1].Colour = AddColours(rows[i + 1][j - 1].Colour, rows[i][j].Colour);
                            break;
                        case Tile.ActionType.Right:
                            if (j >= rows[i].Count - 1) break;
                            if (i == rows.Count - 1)
                            {
                                Results.Add(new Vector2(j + 1, (int)rows[i][j].Colour));
                                break;
                            }
                            rows[i + 1][j + 1].Colour = AddColours(rows[i + 1][j + 1].Colour, rows[i][j].Colour);
                            break;
                    }
                }
            }
        }

        if (Results.Count != currentLevel.goals.Count)
        {
            //Failed
            Failed();
            return;
        }

        List<bool> results = new List<bool>(currentLevel.goals.Count);

        for (int i = 0; i < currentLevel.goals.Count; i++)
        {
            results.Add(false);

            for (int j = 0; j < Results.Count; j++)
            {
                if (currentLevel.goals[i].position == Mathf.RoundToInt(Results[j].x))
                {
                    if (currentLevel.goals[i].colourIndex == Mathf.RoundToInt(Results[j].y))
                    {
                        results[i] = true;
                    }
                }
            }
        }

        if (results.Contains(false))
        {
            //Failed
            Failed();
        }
        else
        {
            //Success
            Success();
        }
    }

    public void Reset ()
    {
        //Reset the board
        for (int i = 0; i < rows.Count; i++)
        {
            for (int j = 0; j < rows[i].Count; j++)
            {
                for (int k = 0; k < rows[i][j].transform.childCount; k++)
                {
                    Destroy(rows[i][j].transform.GetChild(k).gameObject);
                }

                rows[i][j].Colour = LightColour.None;
                rows[i][j].Action = Tile.ActionType.None;
            }
        }

        Results.Clear();

        if (hasWon)
        {
            Destroy(currentLevel.gameObject);

            //new level
            currentLevel = new GameObject().AddComponent<Level>();
            currentLevel.name = "Level";

            hasWon = false;
        }

        hasWon = false;

        CheckComplete();
    }

    void Success ()
    {
        hasWon = true;
        if (SubmitBtn != null) SubmitBtn.GetComponentInChildren<Text>().text = nextText;
    }

    void Failed ()
    {
        if (SubmitBtn != null) SubmitBtn.GetComponentInChildren<Text>().text = resetText;
    }

    private LightColour AddColours (LightColour lhs, LightColour rhs)
    {
        if (lhs == LightColour.None) return rhs;
        if (rhs == LightColour.None) return lhs;

        int sum = (int)lhs + (int)rhs;

        //blue,red,yellow (4,5,6)
        if ((int)lhs < 4) return lhs;
        if ((int)rhs < 4) return rhs;

        if (sum == 9) return LightColour.Purple;//blue and red
        if (sum == 10) return LightColour.Green;//blue and yellow
        if (sum == 11) return LightColour.Orange;//Red and yellow

        return LightColour.White;
    }

	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        }
    }
}
